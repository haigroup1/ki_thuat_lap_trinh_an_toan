package com.kma.ktltat.model;


import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;

import java.io.Serializable;
import java.util.Map;

public class ApiResponse implements Serializable {
    private static final long serialVersionUID = 5926468583005150707L;
    private String code;
    private String message;
    private Map<String, Object> response;


    public ApiResponse() {
    }

    public ApiResponse(String code, String message, Map<String, Object> response) {
        this.code = code;
        this.message = message;
        this.response = response;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getResponse() {
        return response;
    }

    public void setResponse(Map<String, Object> response) {
        this.response = response;
    }
}
