package com.kma.ktltat.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kma.ktltat.entity.Company;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;


public class EmployeeRes implements Serializable {
    @JsonProperty(value = "id")
    private Integer ide;

    @JsonProperty(value = "fullname")
    private String nameE;

    private String email;

    private String phone;

    private String birthday;

    private String user_role;

    public EmployeeRes() {
    }

    public EmployeeRes(Integer ide, String nameE, String email, String phone, String birthday, String user_role) {
        this.ide = ide;
        this.nameE = nameE;
        this.email = email;
        this.phone = phone;
        this.birthday = birthday;
        this.user_role = user_role;
    }

    public Integer getIde() {
        return ide;
    }

    public void setIde(Integer ide) {
        this.ide = ide;
    }

    public String getNameE() {
        return nameE;
    }

    public void setNameE(String nameE) {
        this.nameE = nameE;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getUser_role() {
        return user_role;
    }

    public void setUser_role(String user_role) {
        this.user_role = user_role;
    }
}
