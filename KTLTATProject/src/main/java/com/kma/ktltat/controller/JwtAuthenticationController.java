package com.kma.ktltat.controller;

import com.kma.ktltat.model.ApiResponse;
import com.kma.ktltat.security.JBcryptHelper;
import com.kma.ktltat.service.impl.JwtTokenUtil;
import com.kma.ktltat.entity.Employee;
import com.kma.ktltat.entity.JwtRequest;
import com.kma.ktltat.entity.JwtResponse;
import com.kma.ktltat.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin
public class JwtAuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JBcryptHelper jBcryptHelper;


    @PostMapping(value = "/login")
    public ResponseEntity<?> createAuthenticationToken( @RequestBody JwtRequest authenticationRequest) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        Map<String, Object> body = new HashMap<>();
        try {
            //authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
            String email = authenticationRequest.getEmail();
            String planTextPassword = authenticationRequest.getPassword();

            //Lấy employee từ email
            Employee employee = employeeService.loadEmployeeFromEmail(email);
            //Công chuỗi plantext và hash
            String hashPassword = planTextPassword + employee.getPassword_salt();
            if (jBcryptHelper.checkBcryptPassword(hashPassword, employee.getPassworde())) {
                String token = jwtTokenUtil.generateToken(employee);
                apiResponse.setCode("00");
                apiResponse.setMessage("Đăng nhập thành công");
                body.put("data_res", employee);
                body.put("token", new JwtResponse(token));
                apiResponse.setResponse(body);
                return new ResponseEntity<>(apiResponse, HttpStatus.OK);
            }
            apiResponse.setCode("01");
            apiResponse.setMessage("Sai tài khoản hoặc mật khẩu");
            return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
        }catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Error in createAuthenticationToken Controller");
            apiResponse.setCode("01");
            apiResponse.setMessage("Exception");
            return new ResponseEntity<>(apiResponse, HttpStatus.CONFLICT);
        }
    }

    private void authenticate(String email, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
