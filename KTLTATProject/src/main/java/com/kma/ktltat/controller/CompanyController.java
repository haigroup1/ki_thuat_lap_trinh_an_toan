package com.kma.ktltat.controller;

import com.kma.ktltat.entity.AmountCompany;
import com.kma.ktltat.entity.Company;
import com.kma.ktltat.entity.Employee;
import com.kma.ktltat.service.CompanyService;
import com.kma.ktltat.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CompanyController {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private EmployeeService employeeService;


    @GetMapping("/listemployee")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Employee> getAllEmployee() {
        List<Employee> listEmployee = employeeService.getAllEmployee();
        return listEmployee;
    }

    @GetMapping("/company")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Employee> getAllEmployeeByIdc( @RequestParam Integer idC ) {
        List<Employee> listEmployeeOfCompany = employeeService.findByIdcCompany(idC);
        return listEmployeeOfCompany;
    }

    @GetMapping("/amount")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<?> getAmountEmployeeOfCompany( ) {
        List<Company> companys = companyService.getCompanys();
        List<AmountCompany> amountCompanys = new ArrayList<AmountCompany>();
        for(int i = 0; i < companys.size(); i++) {
            Integer amount = companyService.countAllByIdc(companys.get(i).getIdC());
            AmountCompany amountCompany = new AmountCompany(companys.get(i).getIdC(), amount, companys.get(i).getNameC());
            amountCompanys.add(amountCompany);
        }
        return ResponseEntity.ok(amountCompanys);
    }


    @GetMapping("/")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Company> getCompany() {
        List<Company> company = companyService.getCompanys();
        return company;
    }

}
