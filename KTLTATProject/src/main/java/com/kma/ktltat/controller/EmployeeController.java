package com.kma.ktltat.controller;

import com.kma.ktltat.entity.Employee;
import com.kma.ktltat.entity.JwtResponse;
import com.kma.ktltat.model.ApiResponse;
import com.kma.ktltat.model.EmployeeRes;
import com.kma.ktltat.security.JBcryptHelper;
import com.kma.ktltat.service.EmployeeService;
import com.kma.ktltat.service.impl.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

//Responsebody + Controller
@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JBcryptHelper jBcryptHelper;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    //RequestMapping+ method(GET)
    @GetMapping(value = "/employee")
    public ResponseEntity<?> getAllEmployee(@RequestParam String email) {
        ApiResponse apiResponse = new ApiResponse();
        Map<String, Object> body = new HashMap<>();
        try {
            Employee employee = employeeService.loadEmployeeFromEmail(email);
            if(employee == null) {
                apiResponse.setMessage("Không tìm thấy ông "+ email);
                apiResponse.setCode("01");
                return new ResponseEntity<>(apiResponse, HttpStatus.OK);
            }
            EmployeeRes employeeRes = new EmployeeRes(employee.getIdE(), employee.getNameE(), employee.getEmail(), employee.getPhone(), employee.getBirthday(), employee.getUser_role());
            apiResponse.setMessage("Lấy thông tin người dùng "+ email+" thành công");
            apiResponse.setCode("00");
            body.put("data_res", employeeRes);
            apiResponse.setResponse(body);
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }catch (Exception ex) {
            ex.printStackTrace();
            apiResponse.setMessage("Lấy thông tin người dùng thất bại");
            apiResponse.setCode("01");
            body.put("data_res", ex.getMessage());
            apiResponse.setResponse(body);
            return new ResponseEntity<>(apiResponse, HttpStatus.CONFLICT);
        }
    }

    @PostMapping(value = "/employee")
    public ResponseEntity<?> addEmployee(@Valid @RequestBody  Employee employee ) {
        ApiResponse apiResponse = new ApiResponse();
        Map<String, Object> body = new HashMap<>();
        try {
                Employee employeeCheck = employeeService.loadEmployeeFromEmail(employee.getEmail());
                if(employeeCheck!= null ) {
                    apiResponse.setMessage("Email đã tồn tại");
                    apiResponse.setCode("01");
                    return new ResponseEntity<>(apiResponse, HttpStatus.OK);
                }
                employeeCheck = employeeService.saveEmployee(employee);
                if(employeeCheck == null ) {
                    apiResponse.setMessage("Tạo mới không thành công");
                    apiResponse.setCode("01");
                    return new ResponseEntity<>(apiResponse, HttpStatus.OK);
                }
                String totalPassword[] = jBcryptHelper.generatePasswordByHashPlantextAndSalt(employee.getPassworde());
                employeeCheck.setPassworde(totalPassword[0]);
                employeeCheck.setPassword_salt(totalPassword[1]);
                String token = jwtTokenUtil.generateToken(employeeCheck);
                body.put("data_res", employee);
                body.put("token", new JwtResponse(token));
                apiResponse.setResponse(body);
                apiResponse.setMessage("Tạo mới thành công");
                apiResponse.setCode("00");
                return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }catch (Exception ex ) {
            ex.printStackTrace();
            apiResponse.setMessage("Tạo mới không thành công");
            apiResponse.setCode("01");
            body.put("data_res", ex.getMessage());
            apiResponse.setResponse(body);
            return new ResponseEntity<>(apiResponse, HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping(value = "/employee")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Boolean> deleteEmployee(@RequestParam Integer idE) {
        try {
            employeeService.deleteEmployee(idE);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(false, HttpStatus.CONFLICT);
        }
    }

    @PutMapping(value = "/employee")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Boolean> updateEmployee( @RequestParam Integer idE,@Valid @RequestBody  Employee employee) {
        try {
            Optional<Employee> currentEmployee = employeeService.findUserById(idE);
            if ((!currentEmployee.isPresent())) {
                System.out.println("Không tìm được nhân viên cần sửa");
                return new ResponseEntity<>(false, HttpStatus.CONFLICT);
            }
            employee.setIdE(idE);
            employee.setPassworde(currentEmployee.get().getPassworde());

            employeeService.saveEmployee(employee);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(false, HttpStatus.CONFLICT);
        }
    }
}
