package com.kma.ktltat.service;

import com.kma.ktltat.entity.Company;

import java.util.List;
import java.util.Optional;

public interface CompanyService {
    public Optional<Company> getCompany ( Integer idC);

    public List<Company> getCompanys();

    public Integer countAllByIdc( Integer idc);
}
