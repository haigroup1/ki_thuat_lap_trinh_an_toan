package com.kma.ktltat.service.impl;

import com.kma.ktltat.entity.Company;
import com.kma.ktltat.repository.CompanyRepository;
import com.kma.ktltat.repository.EmployeeRepository;
import com.kma.ktltat.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Optional<Company> getCompany ( Integer idC) {
        return companyRepository.findById(idC);
    }

    @Override
    public List<Company> getCompanys () {
        return companyRepository.findAll();
    }

    @Override
    public Integer countAllByIdc ( Integer idc ) {
        return companyRepository.countAllByIdc(idc);
    }


}
