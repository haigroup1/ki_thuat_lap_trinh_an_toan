package com.kma.ktltat.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "company")
public class Company {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(value = "id")
    private Integer idc;

    @Column(name = "namec")
    @JsonProperty(value = "name_company")
    private String nameC;

    @Column(name = "descriptionc")
    @JsonProperty(value = "description")
    private String descriptionC;

    @Column(name = "addressc")
    @JsonProperty(value = "address")
    private String addressC;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")

    private Set<Employee> listEmployee = new HashSet<Employee>();


    public Company () {
    }

    public Company ( String nameC, String descriptionC, String addressC ) {
        this.nameC = nameC;
        this.descriptionC = descriptionC;
        this.addressC = addressC;
    }

    public Integer getIdC () {
        return idc;
    }

    public void setIdC ( Integer idC ) {
        this.idc = idC;
    }

    public String getNameC () {
        return nameC;
    }

    public void setNameC ( String nameC ) {
        this.nameC = nameC;
    }

    public String getDescriptionC () {
        return descriptionC;
    }

    public void setDescriptionC ( String descriptionC ) {
        this.descriptionC = descriptionC;
    }

    public String getAddressC () {
        return addressC;
    }

    public void setAddressC ( String addressC ) {
        this.addressC = addressC;
    }
}

