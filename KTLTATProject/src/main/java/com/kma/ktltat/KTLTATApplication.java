package com.kma.ktltat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KTLTATApplication {

    public static void main ( String[] args ) {
        SpringApplication.run(KTLTATApplication.class, args);
    }

}
