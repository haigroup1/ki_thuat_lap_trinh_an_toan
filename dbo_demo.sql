/*
Navicat SQL Server Data Transfer

Source Server         : LocalHost
Source Server Version : 140000
Source Host           : localhost:1433
Source Database       : Savid1
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 140000
File Encoding         : 65001

Date: 2021-09-09 21:28:19
*/


-- ----------------------------
-- Table structure for Company
-- ----------------------------
DROP TABLE [dbo].[Company]
GO
CREATE TABLE [dbo].[Company] (
[idc] int NOT NULL IDENTITY(1,1) ,
[namec] nvarchar(255) NOT NULL ,
[descriptionc] ntext NULL DEFAULT ('Ðây là miêu t? c?a công ty') ,
[addressc] nvarchar(255) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Company]', RESEED, 2)
GO

-- ----------------------------
-- Records of Company
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Company] ON
GO
INSERT INTO [dbo].[Company] ([idc], [namec], [descriptionc], [addressc]) VALUES (N'1', N'Savis Viet Nam', N'Công ty công nghệ hỗ trợ giải pháp phần mềm cho các tổ chức', N'Hà Nội')
GO
GO
INSERT INTO [dbo].[Company] ([idc], [namec], [descriptionc], [addressc]) VALUES (N'2', N'Savis tại Singapo', N'Cũng là Savis nhưng mà ở Singapo', N'Hà Nội')
GO
GO
SET IDENTITY_INSERT [dbo].[Company] OFF
GO

-- ----------------------------
-- Table structure for Employee
-- ----------------------------
DROP TABLE [dbo].[Employee]
GO
CREATE TABLE [dbo].[Employee] (
[ide] int NOT NULL IDENTITY(1,1) ,
[idc] int NULL ,
[namee] nvarchar(255) NULL ,
[email] nvarchar(255) NULL ,
[phone] nvarchar(255) NULL ,
[birthday] date NULL ,
[passwordE] nvarchar(255) NULL ,
[password_salt] nvarchar(20) NULL ,
[user_role] nvarchar(30) NULL DEFAULT ('user') 
)


GO
DBCC CHECKIDENT(N'[dbo].[Employee]', RESEED, 22)
GO

-- ----------------------------
-- Records of Employee
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Employee] ON
GO
INSERT INTO [dbo].[Employee] ([ide], [idc], [namee], [email], [phone], [birthday], [passwordE], [password_salt], [user_role]) VALUES (N'1', N'1', N'Trần Văn Hải', N'tranhaj2024@gmail.com', N'0354205023', N'1999-02-25', N'$2a$06$rxvVLEfPTQqWdx7lPHy8EurK3dt69Ys93.Gxpqlmkc17MRMVbhi2K', N'9wsyLL', N'admin')
GO
GO
INSERT INTO [dbo].[Employee] ([ide], [idc], [namee], [email], [phone], [birthday], [passwordE], [password_salt], [user_role]) VALUES (N'3', N'2', N'Nguyễn Văn Bách', N'bach@gmail.com', N'12345689', N'2020-06-24', N'$2a$06$x6eVmgIrjslwX7Rw6vb.X.fJhXn3MH1KUcv4i.RJUG1R9g/pAtRTO', N'U409r8', N'user')
GO
GO
INSERT INTO [dbo].[Employee] ([ide], [idc], [namee], [email], [phone], [birthday], [passwordE], [password_salt], [user_role]) VALUES (N'5', N'2', N'Nguyễn Văn B', N'abc@gmail.com', N'123456890', N'2020-06-24', N'$2a$06$IFhEXIaqRtwJTPcsoiMTV.CYtjItTzhq4kvAvnpBbSS8gr0EWNyv.', N'NxeTTm', N'user')
GO
GO
INSERT INTO [dbo].[Employee] ([ide], [idc], [namee], [email], [phone], [birthday], [passwordE], [password_salt], [user_role]) VALUES (N'7', N'2', N'Nguyễn Văn B', N'abcd@gmail.com', N'0912345678', N'2020-06-24', N'$2a$06$.BeSQ8Ea3A2/nD0AGAY5uesfQdtXd9Adw/lLb2fSPTdanIJtO8.Dy', N'4LEcqT', N'user')
GO
GO
INSERT INTO [dbo].[Employee] ([ide], [idc], [namee], [email], [phone], [birthday], [passwordE], [password_salt], [user_role]) VALUES (N'19', N'2', N'Trần Văn Hải', N'tranha2j2024@gmail.com', N'03542205023', N'2020-07-22', N'$2a$06$beHbkjTLdH.ZmkMTTKYSXOV9xl.IjnCknyCsZ5YuF2MOWt1hMveLO', N'MiJtdp', null)
GO
GO
INSERT INTO [dbo].[Employee] ([ide], [idc], [namee], [email], [phone], [birthday], [passwordE], [password_salt], [user_role]) VALUES (N'21', N'2', N'Trần Văn H', N'tran4haj2024@gmail.com', N'03542050423', N'2020-07-29', N'$2a$06$NvcznGn1HOsyDNux.MWy2ONtVTKpLOUSCjgecizqkLYbDREHlLlbe', N'udOMCO', null)
GO
GO
INSERT INTO [dbo].[Employee] ([ide], [idc], [namee], [email], [phone], [birthday], [passwordE], [password_salt], [user_role]) VALUES (N'22', N'1', N'Trần Văn T', N'tranh3aj2024@gmail.com', N'03541205023', N'2020-07-22', N'$2a$06$n9JiiVVDxzK4RaqjnmIgAuKCwN5PkcHCW.IKG/f9WLWTAzXwcItuW', N'Uy2bRF', null)
GO
GO
SET IDENTITY_INSERT [dbo].[Employee] OFF
GO

-- ----------------------------
-- Indexes structure for table Company
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Company
-- ----------------------------
ALTER TABLE [dbo].[Company] ADD PRIMARY KEY ([idc])
GO

-- ----------------------------
-- Indexes structure for table Employee
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Employee
-- ----------------------------
ALTER TABLE [dbo].[Employee] ADD PRIMARY KEY ([ide])
GO

-- ----------------------------
-- Uniques structure for table Employee
-- ----------------------------
ALTER TABLE [dbo].[Employee] ADD UNIQUE ([email] ASC)
GO
ALTER TABLE [dbo].[Employee] ADD UNIQUE ([phone] ASC)
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Employee]
-- ----------------------------
ALTER TABLE [dbo].[Employee] ADD FOREIGN KEY ([idc]) REFERENCES [dbo].[Company] ([idc]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
